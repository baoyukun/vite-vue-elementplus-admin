import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// @Built by JustTwo
// author: "JustTwo"
export default defineConfig({
  plugins: [vue()],
})
